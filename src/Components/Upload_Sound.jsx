import React, { Component } from 'react';
import {useCallback} from 'react';
import Dropzone from 'react-dropzone';
import {useDropzone} from 'react-dropzone';

const fileMaxSize = 32000000 // in bytes about 3oMB
// const acceptedFileTypes = 'image/x-png, image/png, image/jpg, image/jpeg, image/gif'
// const acceptedFileTypesArray = acceptedFileTypes.split(",").map((item) => {return item.trim()})

class UploadSound extends Component {
    handleOnDrop = (files, rejectedFiles) => {
        var file   = files.originalEvent.dataTransfer.files[0],
        reader = new FileReader();
        reader.onload = () => {
            const fileAsBinaryString = reader.result;
            // do whatever you want with the file content
            
            console.log(fileAsBinaryString)
            };
        reader.onabort = () => console.log('file reading was aborted');
        reader.onerror = () => console.log('file reading has failed');

        reader.onloadend = function (event) {
            console.log(file);
            // filename is in file.name
            // ... do something here
        }

        reader.readAsArrayBuffer(file);
        files.forEach(file => reader.readAsBinaryString(file))
        if (rejectedFiles && rejectedFiles.length > 0){
            console.log(rejectedFiles);
        }


        if (files && files.length > 0){
            console.log(files);
        }
    }
    render () {
        
      
    return (
      <div>
          <section>
        <h1>Drag and Drop</h1>
        <Dropzone  onDrop={this.handleOnDrop}  multiple={true} maxSize={fileMaxSize}>
        {dropzoneProps => {
    return (
      <div>
        <p>Drop some files here</p>
      </div>
    );
  }}
        </Dropzone>
       
        </section>
      </div>
    )
  }
}

export default UploadSound;
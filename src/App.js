import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import {BrowserRouter as Router, Route } from 'react-router-dom';
import Gantt from './Components/GC';
import Home from './Components/Home';
import GE from './Components/gantexample';
class App extends Component {
  constructor(props){
    super(props);
    
}
  render() {
    let {title} = this.props;
    let {bg} = this.props;
    if(title === undefined ) {
      console.log('undefined props');
      title="React";
      
    }
    if(bg === undefined){
      console.log('undefined props');
      // title="React";
      bg= "App-header"
    } 
    else {
      console.log('defined props');
      console.log(bg);
    }
    return (
    
      <Router>
      <div>
        <Route exact path="/" component={Home}/>
        <Route  path="/news" component={Gantt}/>
        
        <Route  path="/time" component={GE}/>
        </div>
      </Router>
    );
  }
}

export default App;
